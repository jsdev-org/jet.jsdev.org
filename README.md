# [jet.jsdev.org](https://jet.jsdev.org) source codes

<br/>

### Run jet.jsdev.org on localhost

    # vi /etc/systemd/system/jet.jsdev.org.service

Insert code from jet.jsdev.org.service

    # systemctl enable jet.jsdev.org.service
    # systemctl start jet.jsdev.org.service
    # systemctl status jet.jsdev.org.service

http://localhost:4066
