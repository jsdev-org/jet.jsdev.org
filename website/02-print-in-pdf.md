---
layout: page
title: Materials About Printing Oracle Jet Graphs in PDF
permalink: /print-in-pdf/
---


# Materials About Printing Oracle Jet Graphs in PDF

**Generating PDF files is one of the things that Oracle JET does not offer!**


Power Up Your Oracle JET — Generating PDFs  (pdfmake)  
https://medium.com/enpit-developer-blog/power-up-your-oracle-jet-generating-pdfs-b73f3ef7180b


The Travelcost App — An Oracle JET Success Story  
https://medium.com/enpit-developer-blog/the-travelcost-app-an-oracle-jet-success-story-3cb5fec1cd08

<br/>

My Question about printing:  
https://community.oracle.com/message/14647105#14647105


<br/>

**Recommended libraries for printing: (i did not check)**

- pdfjs  
- jsPDF  


<!--

yo oraclejet:app --template basic pdfmake-example
cd pdfmake-example

npm install --save pdfmake


$ vi scripts/grunt/config/oraclejet-build.js
 -->
