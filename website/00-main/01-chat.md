---
layout: page
title: Chat
permalink: /chat/
---

# Chat

<br/>

### Telegram messenger channel

https://t.me/jsdev_org


<br/>

### Gitter Chat (Need github or twitter account to enter)

<a href="https://gitter.im/jsdev-org/Lobby" rel="nofollow"><img src="https://badges.gitter.im/jsdev-org/Lobby.svg" alt="jsdev chat room"></a>

