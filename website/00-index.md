---
layout: page
title: Oracle Jet
permalink: /
---

# Oracle Jet

<br/>

Hi, I am software developer from Russia. Our development team develops application on Oracle ADF. And at least 1 year we replaced slow Oracle ADF components on Oracle JET components. Here i am plan to collect interesting (for me) materials what can help to develop.

<br/>

If you agree your experience with us or want to discuss anything, please go to the <a href="/chat/">chat</a>.



<br/>

### FREE Webinar Online: Oracle MOOC: Soar higher with Oracle JavaScript Extension Toolkit (JET) 4.0 (2018):

<br/>

February 26, 2018 08:00 AM US/Pacific

<br/>

<div align="center">


    <iframe width="560" height="315" src="https://www.youtube.com/embed/jiZh6ERmRkY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

</div>


<br/>

<a href="/video-courses/2018/oracle-jet-4.x/"><strong>Link</strong></a>



<br/>

### Components:

<ul>
    <li><a href="http://www.oracle.com/webfolder/technetwork/jet/jetCookbook.html?component=home&demo=rootCollections">Components</a></li>
</ul>


<br/>

### Examples:

http://www.oracle.com/webfolder/technetwork/jet/public_samples/WorkBetter/public_html/index.html  <br/>
http://www.oracle.com/webfolder/technetwork/jet/globalExamples.html  
