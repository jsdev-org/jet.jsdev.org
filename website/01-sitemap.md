---
layout: page
title: Site Map
permalink: /sitemap/
---

# Site Map

### Loading Oracle JET from CDN

https://docs.oracle.com/middleware/jet400/jet/developer/GUID-219A636B-0D0B-4A78-975B-0528497A82DD.htm#JETDG-GUID-219A636B-0D0B-4A78-975B-0528497A82DD


<br/>

### Getting data from REST service:

[Rest](/rest/)  



<br/>

### Materials About Printing Oracle Jet Graphs in PDF:

[Print in PDF](/print-in-pdf/)  


<br/>

### Books:

[Books](/books/)  


<br/>

### Videos (2018)

<a href="/video-courses/2018/oracle-jet-4.x/">MOOC Oracle JET 4.x</a>



<br/>

### Oracle JET Coding standards eslint config files:

https://github.com/oracle/eslint-config-oraclejet


<br/>

### Oracle JavaScript Extension Toolkit (JET) JavaScript API Reference:

http://www.oracle.com/webfolder/technetwork/jet/jsdocs/index.html

<br/>

### JavaScript Extension Toolkit (JET) Developing Applications with Oracle JET

https://docs.oracle.com/middleware/jet410/jet/developer/toc.htm

<br/>

### Examples:

http://www.oracle.com/webfolder/technetwork/jet/public_samples/WorkBetter/public_html/index.html
<br/>
http://www.oracle.com/webfolder/technetwork/jet/globalExamples.html  


<br/>

### Step-by-Step guide 2017

https://github.com/peppertech/HOL2017/tree/master/beginners-start

<br/>

### Components (Cookbook):

<ul>
    <li><a href="http://www.oracle.com/webfolder/technetwork/jet/jetCookbook.html?component=home&demo=rootCollections">Components</a></li>
</ul>


<br/>

### Theme Builder

Oracle JET framework offers a Theme Builder application to let us visually
customize the configurations we want to have, component wise, in our application.

You can refer to the global examples of custom themes in Oracle JET
at:  
http://www.oracle.com/webfolder/technetwork/jet/globalExamples-ThemeViewer.html


Theme Builder for Oracle JET is available online at:  
http://www.oracle.com/webfolder/technetwork/jet-320/public_samples/JET-Theme-Builder/public_html/index.html.


<br/>

### buil

    $ ojet build --release
    
