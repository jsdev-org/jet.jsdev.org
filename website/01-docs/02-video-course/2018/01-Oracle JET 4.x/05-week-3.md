---
layout: page
title:  MOOC Oracle JET 4.x (2018) week 3
permalink: /video-courses/2018/oracle-jet-4.x/week-3/
---

<br/>

# MOOC Oracle JET 4.x (2018) week 3


<br/>

**Oracle JET 4.x - Lesson 3 - Part 1: Composite Component Architecture**

<br/>

Overview of the third week of the course and an introduction to CCA components and the Composite Component Architecture.

<br/>

<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/AItPsjnfM2E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>

**Oracle JET 4.x - Lesson 3 - Part 2: Interaction with Oracle Cloud Services**

<br/>

How to integrate (or relate to) Oracle Cloud services and technologies, e.g., Oracle Mobile Cloud Service, Oracle Application Container Cloud Service, Oracle Java Cloud Service, Oracle Visual Builder Cloud Service, and Oracle REST Data Service.

<br/>

<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/En55-NTieEU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>


<br/>

**Oracle JET 4.x - Lesson 3 - Part 3: Accessibility**

<br/>

How to solve problems for users with disabilities, and an examination of the related features in Oracle JET, including the Oracle JET Developer Guide.

<br/>

<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/M4a4aHVT_Og" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>

**Oracle JET 4.x - Lesson 3 - Part 4: Internationalization**

<br/>

How to internationalize Oracle JET applications and an examination of the related features in Oracle JET, including the Oracle JET Developer Guide.

<br/>

<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/CzDPjNISw00" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>

**Oracle JET 4.x - Lesson 3 - Part 5: Security**

<br/>

How to use oAuth and related security/authentication techniques in Oracle JET applications, and an examination of the related features in Oracle JET, including the Oracle JET Developer Guide.

<br/>

<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/OU5sj20HVh8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>

**Oracle JET 4.x - Lesson 3 - Part 6: Performance Optimization**

<br/>

How to optimize the performance of a variety of Oracle JET components and the application as a whole.

<br/>

<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/cVBgopjotCE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>

**Oracle JET 4.x - Lesson 3 - Part 7: Testing and Debugging**

<br/>

An introduction to testing frameworks with Oracle JET and how to debug Oracle JET applications.

<br/>

<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/es8bllsSb-I" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>


<br/>

**Oracle JET 4.x - Lesson 3 - Part 8: Building**

<br/>

How to use build features in Oracle JET.

<br/>

<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/QPMlheOYFPQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>

**Oracle JET 4.x - Lesson 3 - Part 9: Packaging and Deployment**

<br/>

How to wrap up development and distribute Oracle JET applications.

<br/>

<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/y0HtxpOTBlU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>

**Oracle JET 4.x - Lesson 3 - Part 10: Contributing to the Oracle JET Community**

<br/>

How to participate in the community and an introduction to social media and other resources focused on Oracle JET.

<br/>

<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/xkxXQQvGCbI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
