---
layout: page
title:  MOOC Oracle JET 4.x (2018) week 1
permalink: /video-courses/2018/oracle-jet-4.x/week-1/
---

<br/>

# MOOC Oracle JET 4.x (2018) week 1


<br/>

**Oracle JET 4.x - Lesson 1 - Part 1: Overview**

<br/>

In this lesson, you will get a basic overview of Oracle JET

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/L2k_qylAtXs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

<br/>


**Oracle JET 4.x - Lesson 1 - Part 2: Modularity**

<br/>

Introduction to RequireJS as a solution to the problems of file loading and modularity, which is presented as one of the key libraries underpinning Oracle JET.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/yznhGmE9VEg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>


<br/>


**Oracle JET 4.x - Lesson 1 - Part 3: Advanced Modularity**

<br/>

Introduction to the Require.js configuration block in the 'main.js' file of applications making use of Oracle JET.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/it9EvsNEPMw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>


**Oracle JET 4.x - Lesson 1 - Part 4: Data Binding**

<br/>

Introduction to Knockout.js as a library for two-way data binding, with a demo showing how it works, presented as one of the key libraries underpinning Oracle JET.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/LNpcpUXlTLM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>


**Oracle JET 4.x - Lesson 1 - Part 5: Oracle JET Components**

<br/>

Introduction to Oracle JET components, as the basis of component based development in Oracle JET applications

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/K_eTX3b_iVk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>


**Oracle JET 4.x - Lesson 1 - Part 6: Loading Resources**

<br/>

Introduction to Require.js Text Plugin Library, which enables the loading of local resources, which is also used in Oracle JET applications to enable the separation of JavaScript files from HTML templates.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/uSqf-BynyUA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>


**Oracle JET 4.x - Lesson 1 - Part 7: Oracle JET Command Line Interface**

<br/>

Introduction to the features and usage of the Oracle JET command-line interface.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/2u75Cry0GDQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>


**Oracle JET 4.x - Lesson 1 - Part 8: Oracle JET Modules**

<br/>

Introduction to the creation of custom Oracle JET modules, consisting of a JavaScript file and an HTML template, with a demo showing how to create them.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/Zm-SEM9DSsE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>


**Oracle JET 4.x - Lesson 1 - Part 9: Oracle JET Cookbook**

<br/>

Introduction to the on-line Oracle JET Cookbook, with steps of how to take several Oracle JET components and integrate them into an Oracle JET application.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/dlqD0K9BKq8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>


<br/>


**Oracle JET 4.x - Lesson 1 - Part 10: Oracle JET Layouts**

<br/>

Introduction to Oracle Alta UI and Flex Layout (part of CSS3), as the central technologies for laying out Oracle JET applications.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/2rsVPtKzMjw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
