---
layout: page
title:  MOOC Oracle JET 4.x (2018)
permalink: /video-courses/2018/oracle-jet-4.x/
---

<br/>

# Oracle MOOC: Soar higher with Oracle JavaScript Extension Toolkit (JET) 4.0 (2018):

<br/>

February 26, 2018 08:00 AM US/Pacific

<br/>

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/jiZh6ERmRkY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

</div>


<br/>

**Description**  


Oracle JavaScript Extension Toolkit (JET) empowers developers by providing a modular open source toolkit based on modern JavaScript, CSS3 and HTML5 design and development principles. Oracle JET is targeted at intermediate to advanced JavaScript developers working on client-side applications. It's a collection of open source JavaScript libraries along with a set of Oracle contributed JavaScript libraries that make it as simple and efficient as possible to build applications that consume and interact with Oracle products and services, especially Oracle Cloud services.

In this three week Massive Open Online Course (MOOC) you will learn how to:

- Apply basic JET principles to create Oracle JET applications including JET modules, layouts and components
- Apply more advanced JET functionality, including navigation, routing, validation, layouts, and responsive design.
- Create hybrid mobile applications with Cordova in combination with Oracle JET.
- Integrate with the world outside Oracle JET, e.g., the Composite Component Architecture (CCA components) and the Oracle Cloud.
- Deal with the remaining enterprise-level challenges, i.e., internationalization, accessibility, and security.

<br/>

**Prerequisites**  


This course is about Oracle JET, not about JavaScript itself and not about the JavaScript ecosystem. You are encouraged to explore the JavaScript language and related tools and technologies to familiarize yourself with them prior to getting started with the course. Also, see the Oracle JET Get Started page (http://www.oracle.com/webfolder/technetwork/jet/globalGetStarted.html) to set up your environment, which will be described in detail during the course.


<br/>

<ul>
    <li><a href="/video-courses/2018/oracle-jet-4.x/week-1/">Week 1</a></li>
    <li><a href="/video-courses/2018/oracle-jet-4.x/week-2/">Week 2</a></li>
    <li><a href="/video-courses/2018/oracle-jet-4.x/week-3/">Week 3</a></li>
</ul>
