---
layout: page
title:  MOOC Oracle JET 4.x (2018) week 2
permalink: /video-courses/2018/oracle-jet-4.x/week-2/
---

<br/>

# MOOC Oracle JET 4.x (2018) week 2



<br/>

**Oracle JET 4.x - Lesson 2 - Part 1: Overview**

<br/>

Overview of the second week of the course, together with a thorough tour through oraclejet.org.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/L9nnqW43ReU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>


<br/>

**Oracle JET 4.x - Lesson 2 - Part 2: Data Visualization Components**

<br/>

Exploration of Oracle JET's component library, while focusing on the Data Visualization components, such as charts and graphs.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/AYkxS7MH_pg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>

**Oracle JET 4.x - Lesson 2 - Part 3: Data Collection Components**

<br/>

Exploration of Data Collection components, especially Oracle JET Table and Oracle JET Tree View.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/ywRyeuY7uQ0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>


<br/>

**Oracle JET 4.x - Lesson 2 - Part 4: Data Sources**

<br/>

Exploration of JSON and REST, together with related solutions, such as $getJSON and the Oracle JET Common Model.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/qe63kQPy8Dc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>


<br/>

**Oracle JET 4.x - Lesson 2 - Part 5: Single Page Applications**

<br/>

Exploration of Oracle JET solutions for Single Page Application development, by using Oracle JET modules to create sections in the page and ojRouter for navigating between them.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/pNZIijIN-fg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>



<br/>

**Oracle JET 4.x - Lesson 2 - Part 6: Validating & Converting Data**

<br/>

Exploration of the validators and converters for error handling on component level and application level in Oracle JET applications.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/XUpB1d1SXIk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>


<br/>

**Oracle JET 4.x - Lesson 2 - Part 7: Layout Techniques**

<br/>

Exploration of the collection of components and patterns for the control of the visual layout and navigation in Oracle JET applications.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/-vX8PWK5KOI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>


<br/>

**Oracle JET 4.x - Lesson 2 - Part 8: Responsive Design Solutions**

<br/>

Exploration of the range of features Oracle JET provides for handling requirements for responsive design, including offcanvas components, Flex Layout, and specific Oracle JET components that help with these concerns.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/giTBTdd8IMs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>


<br/>

**Oracle JET 4.x - Lesson 2 - Part 9: Development Patterns**

<br/>

Exploration of how to put the pieces together from the previous parts in the context of development patterns, which are ways in which applications can be structured for specific purposes.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/FqTpj8lpIDY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>


<br/>

**Oracle JET 4.x - Lesson 2 - Part 10: Hybrid Mobile Application Development**

<br/>

Exploration of the concepts relating to Oracle JET hybrid mobile applications, an investigation of what hybrid mobile applications are, and the workflow of creating/developing these kinds of applications.

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/YdEQ8ATKLbc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
