---
layout: page
title: Oracle JET for Developers - Oracle JET Components – Form Elements, Controls, and Data Collections
permalink: /books/oracle-jet-for-developers/oracle-jet-components/
---

# Oracle JET for Developers: 5. Oracle JET Components – Form Elements, Controls, and Data Collections

<br/>

### Input date

**dashboard.html**

{% highlight html %}

<div class="oj-hybrid-padding">
  <h3>Tests Area</h3>

  <div id="divId">
  	  <label for="joiningDate">Joining Date:</label>
  	  <input id="joiningDate" data-bind="ojComponent:
  	   {component: 'ojInputDate', value: doj}"/>
  	  <br/><br/>
  	  <span class="oj-label">Joining Date value is: </span>
  	  <span data-bind="text: doj"></span>
  	</div>
</div>

{% endhighlight %}

<br/>

<br/>

**dashboard.js**

{% highlight js %}

define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojdatetimepicker', 'ojs/ojselectcombobox',
'ojs/ojtimezonedata'],
function(oj, ko, \$) {

    function SimpleModel() {
        this.doj = ko.observable(oj.IntlConverterUtils.dateToLocalIso

(new Date(2017, 10, 10)));
}

    return new SimpleModel();

}
);

{% endhighlight %}

<br/>
<br/>

Results:

<br/>

<div align="center">
    <img src="/img/books/jet/oracle-jet-for-developers/05-oracle-jet-components/pic1.png" alt="Oracle JET Components">
</div>

<br/>

<div align="center">
    <img src="/img/books/jet/oracle-jet-for-developers/05-oracle-jet-components/pic2.png" alt="Oracle JET Components">
</div>

<br/>

### Select:

{% highlight html %}

<div class="oj-hybrid-padding">
  <h3>Tests Area</h3>

  <form id="form1">
    <label for="selectTransport">Mode of Transport: </label>
    <select id="selectTransport" data-bind="ojComponent:
     {component: 'ojSelect', value: selectedVal,
      rootAttributes: {style:'max-width:20em'}}">
      <option value="W">Walk</option>
      <option value="B">Bicycle</option>
      <option value="M">Motor Cycle</option>
      <option value="C">Car</option>
      <option value="P">Public Transport</option>
    </select>
    <div>
      <br/> <hr/>
      <label for="selectedTransport">Selected Transport value is </label>
      <span id="selectedTransport" data-bind="text: ko.toJSON(selectedVal)"></span>
    </div>
  </form>
</div>

{% endhighlight %}

<br/>

{% highlight js %}

define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojselectcombobox'],
function(oj, ko, \$) {

    function SimpleModel() {
    		 this.selectedVal = ko.observableArray(["C"]);
    }

    return new SimpleModel();

}
);

{% endhighlight %}

<br/>

### multi combo box:

{% highlight html %}

<div class="oj-hybrid-padding">
  <h3>Tests Area</h3>

  <form id="form1">
    <label for="selectTransport">Mode of Transport: </label>
    <select id="selectTransport" data-bind="ojComponent:
     {component: 'ojCombobox', value: selectedVal, multiple:true,
      rootAttributes: {style:'max-width:20em'}}">
      <option >Walk</option>
      <option >Bicycle</option>
      <option >Motor Cycle</option>
      <option >Car</option>
      <option >Public Transport</option>
    </select>
    <div>
      <br/><br/> <hr/><br/><br/>
      <label for="selectedTransport">Selected Transport value is </label>
      <span id="selectedTransport" data-bind="text: ko.toJSON(selectedVal)"></span>
    </div>
  </form>

</div>

{% endhighlight %}

<br/>

{% highlight js %}

define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojselectcombobox'],
function(oj, ko, \$) {

    function SimpleModel() {
    		 this.selectedVal = ko.observableArray(["Bicycle","Car"]);
    }

    return new SimpleModel();

}
);

{% endhighlight %}

<br/>

Other:

https://raw.githubusercontent.com/oracle-jet/Oracle-JET-for-Developers/master/Chapter05/Chapter05_Code.txt
