---
layout: page
title: Oracle JET for Developers - Oracle JET Components – Layouts, Navigation, and Visualizations
permalink: /books/oracle-jet-for-developers/oracle-jet-components-layouts-navigation-visualizations/
---

# Oracle JET for Developers: 5. Oracle JET Components – Layouts, Navigation, and Visualizations

<br/>

### Navigation lists

**dashboard.html**

{% highlight html %}

<div class="oj-hybrid-padding">
  <h3>Tests Area</h3>

  <div id="navlistdemo">
  <div class="oj-flex oj-flex-items-pad">
  	<div class="oj-flex-item">
  	   <label id="navigationLevellabelid">Navigation Level</label>
  	   <div id="navigationLevelRadioId" aria-labelledby="navigationLevellabelid"
  			data-bind="ojComponent: {
  			 component: 'ojRadioset',
  			 value: navigationLevel}" >
  		   <span class="oj-choice-item">
  			   <input id="application" type="radio" name="navigationLevel" value="application">
  			   <label for="application">Application</label>
  		   </span>
  		   <span class="oj-choice-item">
  			   <input id="page" type="radio" name="navigationLevel" value="page">
  			   <label for="page">Page</label>
  		   </span>
  	   </div>
  	</div>
  	<div class="oj-flex-item">
  		<label id="condenseLabel" for="condense">Condense</label>
  		<input id="condense" data-bind="ojComponent: {component: 'ojSwitch', value: isChecked}" />
  	</div>
  	<div class="oj-flex-item">
  		<label id="contrastBgLabel" for="contrastBgSwitch">Contrast Background</label>
  		<input id="contrastBgSwitch" data-bind="ojComponent: {component: 'ojSwitch', value: isContrastBackground}" />
  	</div>
  </div>
  <p>
  <h4>With Only Text </h4>
  	  <div class="navlistcontainer">
  		<div aria-label="Choose a navigation item" data-bind="ojComponent:{
  							component: 'ojNavigationList',
  							drillMode: 'none',
  							selection: 'home5',
  							navigationLevel: navigationLevel,
  							edge: 'top'}" >
  		<ul >
  			<li id="home5">
  			  <a href="#">Home</a>
  			</li>
  			<li id="gettingstarted5">
  			  <a href="#">Getting Started</a>
  			</li>
  			<li id="cookbook5">
  			  <a href="#">Cookbook</a>
  			</li>
  			<li id="stylelab5" class="oj-disabled">
  			  <a href="#">Style Lab</a>
  			</li>
  			<li id="library5" >
  				<a href="#" >Library</a>
  			</li>
  		</ul>
  		</div>
  	  </div>
  <h4>With Text and Icons</h4>
  	  <div class="navlistcontainer">
  		<div aria-label="Choose a navigation item" data-bind="ojComponent:{
  							component: 'ojNavigationList',
  							drillMode: 'none',
  							selection: 'home4',
  							navigationLevel: navigationLevel,
  							edge: 'top'}" >
  		  <ul>
  			  <li id="home4">
  				<a href="#">
  					<span class="oj-navigationlist-item-icon
  								 demo-home-icon-24 demo-icon-font-24">
  					</span>Home</a>
  			  </li>
  			  <li id="gettingstarted4">
  				<a href="#">
  					<span class="oj-navigationlist-item-icon
  								 demo-education-icon-24 demo-icon-font-24">
  					</span>Getting Started</a>
  			  </li>
  			  <li id="cookbook4">
  				<a href="#">
  					<span class="oj-navigationlist-item-icon
  								 demo-catalog-icon-24 demo-icon-font-24">
  					</span>Cookbook</a>
  			  </li>
  			  <li id="stylelab4" class="oj-disabled">
  				<a href="#">
  					<span class="oj-navigationlist-item-icon
  								  demo-palette-icon-24 demo-icon-font-24">
  					</span>Style Lab</a>
  			  </li>
  			  <li id="library4" >
  				  <a href="#" >
  					  <span class="oj-navigationlist-item-icon
  						  demo-library-icon-24 demo-icon-font-24">
  					  </span>Library</a>
  			  </li>
  		  </ul>
  		</div>
  	  </div>
  		<h4> With only Icons</h4>
  		<div class="navlistcontainer">
  		  <div aria-label="Choose a navigation item" data-bind="ojComponent:{
  							  component: 'ojNavigationList',
  							  drillMode: 'none',
  							  display:'icons',
  							  selection: 'home1',
  							  navigationLevel: navigationLevel,
  							  edge: 'top'}">
  			<ul >
  				<li id="home1">
  				  <a href="#">
  					  <span class="oj-navigationlist-item-icon
  								   demo-home-icon-24 demo-icon-font-24">
  					  </span>Home</a>
  				</li>
  				<li id="gettingstarted1">
  				  <a href="#">
  					  <span class="oj-navigationlist-item-icon
  								   demo-education-icon-24 demo-icon-font-24">
  					  </span>Getting Started</a>
  				</li>
  				<li id="cookbook1">
  				  <a href="#">
  					  <span class="oj-navigationlist-item-icon
  								   demo-catalog-icon-24 demo-icon-font-24">
  					  </span>Cookbook</a>
  				</li>
  				<li id="stylelab1" class="oj-disabled">
  				  <a href="#">
  					  <span class="oj-navigationlist-item-icon
  									demo-palette-icon-24 demo-icon-font-24">
  					  </span>Style Lab</a>
  				</li>
  				<li id="library1" >
  					<a href="#" >
  						<span class="oj-navigationlist-item-icon
  							demo-library-icon-24 demo-icon-font-24">
  						</span>Library</a>
  				</li>
  			</ul>
  		  </div>
  	  </div>
  <h4> With Stacked Label and Icon</h4>
  <div class="navlistcontainer">
    <div aria-label="Choose a navigation item" data-bind="ojComponent:{
  					  component: 'ojNavigationList',
  					  drillMode: 'none',
  					  selection: 'home2',
  					  navigationLevel: navigationLevel,
  					  edge: 'top'}" class="oj-navigationlist-stack-icon-label">
  	<ul>
  		<li id="home2">
  		  <a href="#">
  			  <span class="oj-navigationlist-item-icon
  						   demo-home-icon-24 demo-icon-font-24">
  			  </span>Home</a>
  		</li>
  		<li id="gettingstarted2">
  		  <a href="#">
  			  <span class="oj-navigationlist-item-icon
  						   demo-education-icon-24 demo-icon-font-24">
  			  </span>Getting Started</a>
  		</li>
  		<li id="cookbook2">
  		  <a href="#">
  			  <span class="oj-navigationlist-item-icon
  						   demo-catalog-icon-24 demo-icon-font-24">
  			  </span>Cookbook</a>
  		</li>
  		<li id="stylelab2" class="oj-disabled">
  		  <a href="#">
  			  <span class="oj-navigationlist-item-icon
  							demo-palette-icon-24 demo-icon-font-24">
  			  </span>Style Lab</a>
  		</li>
  		<li id="library2" >
  			<a href="#" >
  				<span class="oj-navigationlist-item-icon
  					demo-library-icon-24 demo-icon-font-24">
  				</span>Library</a>
  		</li>
  	</ul>
    </div>
  </div>
  </div>

</div>

{% endhighlight %}

<br/>

<br/>

**dashboard.js**

{% highlight js %}

define([
'ojs/ojcore',
'knockout',
'jquery',
'ojs/ojknockout',
'ojs/ojnavigationlist',
'ojs/ojswitch',
'ojs/ojradioset'
],
function(oj, ko, \$) {

    function SimpleModel() {

    	this.navigationLevel = ko.observable('page');
       	   this.isChecked = ko.observable();
       	   this.isChecked.subscribe(function(newValue) {
       		   var navlistInstances = $('#navlistdemo').find(':oj-navigationlist');
       		   if(newValue) {
       			   navlistInstances.addClass('oj-sm-condense');
       		   } else {
       			   navlistInstances.removeClass('oj-sm-condense');
       		   }
       	   });
       	   this.isContrastBackground = ko.observable(false);
       	   this.isContrastBackground.subscribe(function(newValue) {
       		   if(newValue) {
       			   $(".navlistcontainer").addClass("demo-panel-contrast1 oj-contrast-marker");
       		   } else {
       			   $(".navlistcontainer").removeClass("demo-panel-contrast1 oj-contrast-marker");
       		   }
       	   });

    }

    return new SimpleModel();

}
);

{% endhighlight %}

<br/>
<br/>

Results:

<br/>

<div align="center">
    <img src="/img/books/jet/oracle-jet-for-developers/06-oracle-jet-components/pic1.png" alt="Oracle JET Components">
</div>

<br/>

Other:

https://raw.githubusercontent.com/oracle-jet/Oracle-JET-for-Developers/master/Chapter06/Chapter06_Code.txt
