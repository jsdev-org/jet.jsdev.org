---
layout: page
title: Oracle JET for Developers - Knockout.js
permalink: /books/oracle-jet-for-developers/knokout-js/
---

# Oracle JET for Developers: 4. Knockout.js

<br/>

{% highlight html %}

<!DOCTYPE html>

       <html>
         <head>
           <title>Knockout JS</title>
         </head>
         <body>
           <h1>Welcome to Knockout JS programming</h1>
           <table border="1" >
             <tr >
               <th colspan="2" style="padding:10px;">
                 <b>Employee Data from View Model</b>
               </th>
             </tr>
             <tr>
               <td style="padding:10px;">Employee First Name:</td>
               <td style="padding:10px;">
                 <span data-bind='text: empFirstName'></span>
               </td>
             </tr>
             <tr>
               <td style="padding:10px;">Employee Last Name:</td>
               <td style="padding:10px;">
                 <span data-bind='text: empLastName'></span>
               </td>
             </tr>
           </table>
           <!-- JavaScript resources -->
             <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.2/knockout-min.js'></script>
             <script type='text/javascript'>
               var employeeViewModel = {
                 empFirstName: "Tony",
                 empLastName: "Henry"
               };
               ko.applyBindings(employeeViewModel);
           </script>
         </body>
       </html>

{% endhighlight %}

<br/>

<div align="center">
    <img src="/img/books/jet/oracle-jet-for-developers/pic01.png" alt="knokout">
</div>

<br/>

### Observables

{% highlight html %}

<!DOCTYPE html>

        <html>
          <head>
            <title>Knockout JS</title>
          </head>
          <body>
            <h1>Welcome to Knockout JS programming</h1>
            <table border="1" >
              <tr >
                <th colspan="2" style="padding:10px;">
                  <b>Employee Data - Organization :
                    <span style="color:red" data-bind='text: organizationName'></span>
                  </b>
                </th>
              </tr>
              <tr>
                <td style="padding:10px;">Employee First Name:</td>
                <td style="padding:10px;">
                  <span data-bind='text: empFirstName'></span>
                </td>
              </tr>
              <tr>
                <td style="padding:10px;">Employee Last Name:</td>
                <td style="padding:10px;">
                  <span data-bind='text: empLastName'></span>
                </td>
              </tr>
            </table>
            <!-- JavaScript resources -->
            <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.2/knockout-min.js'></script>
            <script type='text/javascript'>
              var employeeViewModel = {
                empFirstName: "Tony",
                empLastName: "Henry",
                organizationName: ko.observable("Sun")
              };
              ko.applyBindings(employeeViewModel);
              employeeViewModel.organizationName("Oracle");
            </script>
          </body>
        </html>

{% endhighlight %}

<br/>

<div align="center">
    <img src="/img/books/jet/oracle-jet-for-developers/pic02.png" alt="knokout">
</div>

<br/>

<br/>

### Computed observable :

{% highlight html %}

<!DOCTYPE html>
<html>
  <head>
    <title>Knockout JS</title>
  </head>
  <body>
    <h1>Welcome to Knockout JS programming</h1>
    <table border="1" >
      <tr>
        <th colspan="2" style="padding:10px;">
          <b>Employee Data - Organization :
            <span style="color:red" data-bind='text: organizationName'></span>
          </b>
        </th>
      </tr>
      <tr>
        <td style="padding:10px;">Employee First Name:</td>
        <td style="padding:10px;">
          <span data-bind='text: empFirstName'></span>
        </td>
        </tr>
        <tr>
          <td style="padding:10px;">Employee Last Name:</td>
          <td style="padding:10px;">
            <span data-bind='text: empLastName'></span>
          </td>
        </tr>
      </table>
      <p>Organization Full Name : <span style="color:red" data-bind='text: orgFullName'></span></p>

      <!-- JavaScript resources -->
      <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.2/knockout-min.js'></script>
      <script type='text/javascript'>
        var employeeViewModel = {
          empFirstName: "Tony",
          empLastName: "Henry",
          organizationName: ko.observable("Sun")
        };
        employeeViewModel.orgFullName = ko.computed(function() {
          return employeeViewModel.organizationName() + " Limited";
        });
        ko.applyBindings(employeeViewModel);
        employeeViewModel.organizationName("Oracle");
      </script>
    </body>

  </html>

{% endhighlight %}

<br/>

<div align="center">
    <img src="/img/books/jet/oracle-jet-for-developers/pic03.png" alt="knokout">
</div>

<br/>

<br/>

### Observable arrays [1]

{% highlight html %}

<!DOCTYPE html>

       <html>
         <head>
           <title>Knockout JS</title>
         </head>
         <body>
           <h1>Welcome to Knockout JS programming</h1>
           <table border="1" >
             <tr >
               <th colspan="2" style="padding:10px;">
                 <b>Employee Data - Organization :
                   <span style="color:red" data-bind='text: organizationName'></span>
                 </b>
               </th>
             </tr>
             <tr>
               <td style="padding:10px;">Employee First Name:</td>
               <td style="padding:10px;">
                 <span data-bind='text: empFirstName'></span>
               </td>
             </tr>
             <tr>
               <td style="padding:10px;">Employee Last Name:</td>
               <td style="padding:10px;">
                 <span data-bind='text: empLastName'></span>
               </td>
             </tr>
           </table>
           <p>Organization Full Name : <span style="color:red" data-bind='text: orgFullName'></span></p>
           <!-- Observable Arrays-->
           <h2>Observable Array Example : </h2>
           <table border="1">
             <thead><tr>
               <th style="padding:10px;">First Name</th>
               <th style="padding:10px;">Last Name</th>
             </tr></thead>
             <tbody data-bind='foreach: organization'>
               <tr>
                 <td style="padding:10px;" data-bind='text: firstName'></td>
                 <td style="padding:10px;" data-bind='text: lastName'></td>
               </tr>
             </tbody>
           </table>

           <!-- JavaScript resources -->
           <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.2/knockout-min.js'></script>
           <script type='text/javascript'>
             function Employee (firstName, lastName) {
               this.firstName = ko.observable(firstName);
               this.lastName = ko.observable(lastName);
             };
             var employeeViewModel = {
               empFirstName: "Tony",
               empLastName: "Henry",
               //Observable
               organizationName: ko.observable("Sun"),
               //Observable Arrays

               organization : ko.observableArray([
                 new Employee("John", "Kennedy"),
                 new Employee("Peter", "Hennes"),
                 new Employee("Richmond", "Smith")
               ])
             };
             //Computed Observable
             employeeViewModel.orgFullName = ko.computed(function() {
               return employeeViewModel.organizationName() + " Limited";
             });
             ko.applyBindings(employeeViewModel);
             employeeViewModel.organizationName("Oracle");

           </script>
         </body>
       </html>

{% endhighlight %}

<br/>

<div align="center">
    <img src="/img/books/jet/oracle-jet-for-developers/pic04.png" alt="knokout">
</div>

<br/>

<br/>

### Observable arrays [2]

{% highlight html %}

<!DOCTYPE html>

        <html>
          <head>
            <title>Knockout JS</title>
          </head>
          <body>
            <h1>Welcome to Knockout JS programming</h1>
            <table border="1" >
              <tr >
                <th colspan="2" style="padding:10px;">
                  <b>Employee Data - Organization :
                    <span style="color:red" data-bind='text: organizationName'></span>
                  </b>
                </th>
              </tr>
              <tr>
                <td style="padding:10px;">Employee First Name:</td>
                <td style="padding:10px;">
                  <span data-bind='text: empFirstName'></span>
                </td>
              </tr>
              <tr>
                <td style="padding:10px;">Employee Last Name:</td>
                <td style="padding:10px;">
                  <span data-bind='text: empLastName'></span>
                </td>
              </tr>
            </table>
            <p>Organization Full Name : <span style="color:red" data-bind='text: orgFullName'></span></p>
            <!-- Observable Arrays-->
            <h2>Observable Array Example : </h2>
              <table border="1">
              <thead><tr>
                <th style="padding:10px;">First Name</th>
                <th style="padding:10px;">Last Name</th>
              </tr></thead>
              <tbody data-bind='foreach: organization'>
                <tr>
                  <td style="padding:10px;" data-bind='text: firstName'></td>
                  <td style="padding:10px;" data-bind='text: lastName'></td>
                </tr>
              </tbody>
            </table>
            <h2>Add New Employee to Observable Array</h2>
              First Name : <input data-bind="value: newFirstName" />
              Last Name : <input data-bind="value: newLastName" />
              <button data-bind='click: addEmployee'>Add Employee</button>
              <!-- JavaScript resources -->
              <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.2/knockout-min.js'></script>
              <script type='text/javascript'>
                function Employee (firstName, lastName) {
                  this.firstName = ko.observable(firstName);
                  this.lastName = ko.observable(lastName);
                };
                this.addEmployee = function() {
                  this.organization.push(new Employee
                       (employeeViewModel.newFirstName(),
                        employeeViewModel.newLastName()));
                };
                var employeeViewModel = {
                  empFirstName: "Tony",
                  empLastName: "Henry",
                  //Observable
                  organizationName: ko.observable("Sun"),
                  newFirstName: ko.observable(""),
                  newLastName: ko.observable(""),
                  //Observable Arrays
                  organization : ko.observableArray([
                    new Employee("John", "Kennedy"),
                    new Employee("Peter", "Hennes"),
                    new Employee("Richmond", "Smith")
                  ])
                };
                //Computed Observable
                employeeViewModel.orgFullName = ko.computed(function() {
                  return employeeViewModel.organizationName() + " Limited";
                });
                ko.applyBindings(employeeViewModel);
                employeeViewModel.organizationName("Oracle");
              </script>
            </body>
          </html>

{% endhighlight %}

<br/>

<div align="center">
    <img src="/img/books/jet/oracle-jet-for-developers/pic05.png" alt="knokout">
</div>

<br/>

<br/>

## Data bindings

Knockout.js has provided three additional data binding abilities:

- Control-flow bindings
- Appearance bindings
- Interactive bindings

<br/>

### Control-flow bindings

Control-flow bindings help us access the data elements based
on a certain condition. The if , if-not , and with are the control-flow bindings available
from the Knockout.js.

{% highlight html %}

<!DOCTYPE html>
<html>
  <head>
    <title>Knockout JS</title>
  </head>
  <body>
    <h1>Welcome to Knockout JS programming</h1>
    <table border="1" >
      <tr >
        <th colspan="2" style="padding:10px;">
          <b>Employee Data - Organization :
            <span style="color:red" data-bind='text: organizationName'></span>
          </b>
        </th>
     </tr>
     <tr>
       <td style="padding:10px;">Employee First Name:</td>
       <td style="padding:10px;">
         <span data-bind='text: empFirstName'></span>
       </td>
       </tr>
       <tr>
         <td style="padding:10px;">Employee Last Name:</td>
         <td style="padding:10px;">
           <span data-bind='text: empLastName'></span>
         </td>
       </tr>
     </table>
     <p>Organization Full Name : <span style="color:red" data-bind='text: orgFullName'></span></p>
     <!-- Observable Arrays-->
     <h2>Observable Array Example : </h2>

     <table border="1">
       <thead><tr>
         <th style="padding:10px;">First Name</th>
         <th style="padding:10px;">Last Name</th>
         <th style="padding:10px;">Age</th>
       </tr></thead>
       <tbody data-bind='foreach: organization'>
         <tr>
           <td style="padding:10px;" data-bind='text: firstName'></td>
           <td style="padding:10px;" data-bind='text: lastName'></td>
           <td data-bind="if: age() > 20" style="color: green;padding:10px;"><span data-bind='text:age'></span></td>
         </tr>
       </tbody>
     </table>
     <!-- with control flow bindings -->
     <p data-bind='with: markedEmployee'>
        Employee <strong data-bind="text: firstName() + ', ' + lastName()"></strong> is marked with the age <strong data-bind='text: age'></strong>.
     </p>
     <h2>Add New Employee to Observable Array</h2>
     First Name : <input data-bind="value: newFirstName" />
     Last Name : <input data-bind="value: newLastName" />
     Age : <input data-bind="value: newEmpAge" />
     <button data-bind='click: addEmployee'>Add Employee</button>
     <!-- JavaScript resources -->
     <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.2/knockout-min.js'></script>
     <script type='text/javascript'>
       function Employee (firstName, lastName,age) {
         this.firstName = ko.observable(firstName);
         this.lastName = ko.observable(lastName);
         this.age = ko.observable(age);
       };
       this.addEmployee = function() {
         this.organization.push(new Employee
             (employeeViewModel.newFirstName(),
              employeeViewModel.newLastName(),
              employeeViewModel.newEmpAge()));
         };
         var employeeViewModel = {
           empFirstName: "Tony",
           empLastName: "Henry",
           //Observable
           organizationName: ko.observable("Sun"),
           newFirstName: ko.observable(""),
           newLastName: ko.observable(""),
           newEmpAge: ko.observable(""),
           //With control flow object
           markedEmployee: ko.observable(new Employee("Garry", "Parks", "65")),
           //Observable Arrays

           organization : ko.observableArray([
             new Employee("John", "Kennedy", "24"),
             new Employee("Peter", "Hennes","18"),
             new Employee("Richmond", "Smith","54")
           ])
         };
         //Computed Observable
         employeeViewModel.orgFullName = ko.computed(function() {
           return employeeViewModel.organizationName() + " Limited";
         });
         ko.applyBindings(employeeViewModel);
         employeeViewModel.organizationName("Oracle");

      </script>
    </body>

  </html>

{% endhighlight %}

<br/>

<div align="center">
    <img src="/img/books/jet/oracle-jet-for-developers/pic06.png" alt="knokout">
</div>

<br/>

### Appearance bindings

Appearance bindings deal with displaying the data from binding elements on view
components in formats such as text and HTML, and applying styles with the help of
a set of six bindings, as follows

<br/>

1 Text: <value> —Sets the value to an element.

{% highlight html %}

 <td data-bind='text: name'></td>
{% endhighlight %}

<br/>

2 HTML: <value> — Sets the HTML value to an element.

{% highlight html %}
//JavaScript:
function Employee(firstname, lastname, age) {
...
this.formattedName = ko.computed(function() {
return "<strong>" + this.firstname() + "</strong>";
}, this);
}

        //Html:
        <span data-bind='html: markedEmployee().formattedName'></span>

{% endhighlight %}

<br/>

3 Visible: <condition> —An element can be shown or hidden based on the
condition.

{% highlight html %}
<td data-bind='visible: age() > 20' style='color: green'> span data-bind='text:age'>
{% endhighlight %}

<br/>

4 CSS: <object> — An element can be associated with a CSS class.

{% highlight html %}

        //CSS:
       .strongEmployee {
         font-weight: bold;
       }


       //HTML:
       <span data-bind='text: formattedName, css: {strongEmployee}'></span>

{% endhighlight %}

<br/>

5 Style: <object> —Associates an inline style to the element.

{% highlight html %}

<span data-bind='text: age, style: {color: age() > 20 ? "green" :"red"}'></span>

{% endhighlight %}

<br/>

6 Style: <object> —Associates an inline style to the element.

{% highlight html %}

<p><a data-bind='attr: {href: featuredEmployee().populatelink}'>View Employee</a></p>

{% endhighlight %}

<br/>

### Interactive bindings

Interactive bindings help the user interact with the form elements to be associated
with corresponding viewmodel methods or events to be triggered in the pages.
Knockout JS supports the following interactive bindings:

<br/>

1 Click: <method> — An element click invokes a ViewModel method.

{% highlight html %}
<button data-bind='click: addEmployee'>Submit</button>
{% endhighlight %}

<br/>

2 Value: <property> — Associates the form element value to the ViewModel
attribute.

{% highlight html %}
<td>Age: <input data-bind='value: age' /></td>
{% endhighlight %}

<br/>

3 Event: <object> —With an user-initiated event, it invokes a method.

{% highlight html %}
<p data-bind='event: {mouseover: showEmployee, mouseout:hideEmployee}'>
Age: <input data-bind='value: Age' />
</p>
{% endhighlight %}

<br/>

4 Submit: <method> —With a form submit event, it can invoke a method.

{% highlight html %}
<form data-bind="submit: addEmployee">
<!—Employee form fields -->
<button type="submit">Submit</button>
</form>
{% endhighlight %}

<br/>

5 Enable: <property> —Conditionally enables the form elements. Example: last
name field is enabled only after adding first name field.

6 Disable: <property> —Conditionally disables the form elements. Example: last
name field is disabled after adding first name:

{% highlight html %}
<p>
Last Name: <input data-bind='value: lastName, disable: firstName' />

   </p>
{% endhighlight %}

<br/>

7 Checked: <property> — Associates a checkbox or radio element to the
ViewModel attribute.:

{% highlight html %}
<p>
<p>Gender: <input data-bind='checked:gender' type='checkbox' /></p>

   </p>
{% endhighlight %}

<br/>

8 Options: <array> —Defines a ViewModel array for the <select> element.

{% highlight html %}
// Javascript:
this.designations = ko.observableArray(['manager', 'administrator']);

       // Html:
         Designation: <select data-bind='options: designations'></select>

{% endhighlight %}

<br/>

- selectedOptions : <array> — Defines the active/selected element from the <select>
  element.

{% highlight html %}
Designation:
<select data-bind='options: designations,
                   optionsText:"Select",
                   selectedOptions:defaultDesignation'>
</select>
{% endhighlight %}

<br/>

- hasfocus : <property> — Associates the focus attribute to the element..

{% highlight html %}
First Name: <input data-bind='value: firstName, hasfocus: firstNameHasFocus' />
{% endhighlight %}

<br/>

## External data access and animations

{% highlight html %}

    <p><button data-bind='click: loadEmployee'>Load Data</button></p>

{% endhighlight %}

{% highlight html %}

    self. loadEmployee = function() {
         $.getJSON("/get-employee", function(data) {
           employeeViewModel.firstName=data.firstName;
         });
     }

{% endhighlight %}
