---
layout: page
title: Oracle JET for Developers Implement client-side JavaScript efficiently for enterprise Oracle applications [Dec 8, 2017]
permalink: /books/oracle-jet-for-developers/
---

# Oracle JET for Developers: Implement client-side JavaScript efficiently for enterprise Oracle applications [Dec 8, 2017]

<br/>

<strong>My src:</strong>

<br/>

https://github.com/oracle-jet/Oracle-JET-for-Developers


<br/>

# Let the study begin


marley/nodejs container  
htp://jsdev.org/env/docker/run-container/linux/

<br/>

    $ project_name=oracle-jet-for-developers

<br/>

    $ project_folder=~/projects/dev/js/jet
    $ echo ${project_folder}/${project_name}
    $ mkdir -p ${project_folder}/${project_name}

<br/>

{% highlight shell linenos %}

$ docker run -it \
-p 80:8080 -p 1337:1337 -p 3000:3000 -p 4000:4000 -p 5000:5000 -p 7000:7000 -p 8000:8000 -p 9000:9000 \
--name ${project_name} \
-v ${project_folder}/${project_name}:/project \
marley/nodejs

{% endhighlight %}


<br/>

+ additional steps

<br/>

    # npm install -g yo grunt-cli
    # npm install -g generator-oraclejet
    # su nodejs

    $ cd /project
    $ yo oraclejet OracleJETSample --template=navdrawer
    $ cd OracleJETSample/
    $ grunt build
    $ grunt serve

    http://localhost:8000/


<br/>

# Let the study begin


<a href="/books/oracle-jet-for-developers/knokout-js/">4. Knockout.js</a>
